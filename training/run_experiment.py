#!usr/bin/env python
"""
Script to run an experiment
"""
import argparse
import json
import importlib
from typing import Dict
import os
import wandb
from training.util import train_model


DEFAULT_TRAINING_ARGS = {
    'batch_size': 64,
    'epochs': 16
}
