import numpy as np
import tensorflow as tf


def keras_to_tflite(*, keras_model_file: str, tflite_model_file: str):
    """ converts keras model to tflite, and save and return the tflite model"""
    keras_model = tf.keras.models.load_model(keras_model_file)
    converter = tf.lite.TFLiteConverter.from_keras_model(keras_model)
    converter.optimization = [tf.lite.Optimize.DEFAULT]
    tflite_model = convert.convert()
    open(tflite_model_file, "wb").write(tflite_model)

    return tflite_model

def main():
    KERAS_MODEL = ""
    TFLITE_MODEL = ""
    
    """ convert KERAS_MODEL to TFLITE_MODEL """
    keras_to_tflite(keras_model_file=KERAS_MODEL,
                    tflite_model_file=TFLITE_MODEL)

    """ use the tflite model for prediction """
    interpreter = tf.lite.Interpreter(TFLITE_MODEL)
    
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    input_array = np.array([1, 2, 3, 4])
    
    interpreter.set_tensor(intput_details[0]['index'], input_array)

    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]['index'])

    return output_data


if __name__ == "__main__":
    main()
