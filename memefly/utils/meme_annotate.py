from typing import Tuple, List
from PIL import Image, ImageFont, ImageDraw
import textwrap


class MemeAnnotate:
    def __init__(self, *, image:str, fillcolor:str='white', shadowcolor:str='black', 
                 font:str='impact.ttf', margin:int=10):
        self.image = Image.open(image)
        self.fillcolor = fillcolor
        self.shadowcolor = shadowcolor
        self.fontsize = 52
        self.font = ImageFont.truetype(font, self.fontsize)
        self.margin = margin

    def annotate(self, *, text:str):
        draw = ImageDraw.Draw(self.image)
        text_width, _ = draw.textsize(text, self.font)

        if text_width > self.image.width:
            top, bottom = self.__split_text(text=text)
            self.image = self.__draw(pos='upper', text=top)
            self.image = self.__draw(pos='bottom', text=bottom)
        else:
            self.image = self.__draw(text=text)

        return self.image

    def show(self):
        self.image.show()
        pass

    def save(self, *, filename:str="out.jpg"):
        self.image.save(filename)
        pass

    def __draw(self, *, pos:str='upper', text:str):
        """
        Draw text on self.image
        """
        image = self.image.copy()

        # wrap text in case if its too long
        texts = self.__wrap_text(text=text)

        for i in range(len(texts)):
            draw = ImageDraw.Draw(image)

            texts[i] = texts[i].upper()
            text_width, text_height = draw.textsize(texts[i], self.font)

            # horizontal center text
            x = (image.width - text_width)/2

            # set vertical text location
            if pos=='upper':
                y = self.margin + text_height*i
            elif pos == 'bottom':
                y = (image.height - text_height*(len(texts)-i)) - self.margin
            else:
                raise SyntaxError

            # draw shadow
            draw.text((x-2, y-2), texts[i], font=self.font, fill=self.shadowcolor)
            draw.text((x-2, y+2), texts[i], font=self.font, fill=self.shadowcolor)
            draw.text((x+2, y-2), texts[i], font=self.font, fill=self.shadowcolor)
            draw.text((x+2, y+2), texts[i], font=self.font, fill=self.shadowcolor)

            # draw fill
            draw.text((x, y), texts[i], font=self.font, fill=self.fillcolor)

        return image

    def __split_text(self, *, text:str) -> Tuple:
        """
        Used by annotate to split text at meta level.
        """
        texts = text.split(' ')
        top = ' '.join(texts[:int(len(texts)/2)])
        bottom = ' '.join(texts[int(len(texts)/2):])
        
        return top, bottom

    def __wrap_text(self, *, text:str) -> List:
        """
        Used by self.draw to wrap text in case its too long.
        """
        draw = ImageDraw.Draw(self.image)
        text_width, _ = draw.textsize(text, self.font)
        char_width = text_width / len(text)
        row_width = int((self.image.width - char_width*3)/char_width)
        texts = textwrap.wrap(text, width=row_width)

        return texts

    def __find_fontsize(self, *, text:str):
        raise NotImplementedError


def main():
    img = "../../datasets/baby-yoda.jpg"
    text = " like to be paid president until twitter r s first page"
    meme_img_obj = MemeAnnotate(image=img)
    meme_image = meme_img_obj.annotate(text=text)
    meme_img_obj.show()
    meme_img_obj.save(filename='out4.jpg')

if __name__ == "__main__":
    main()
