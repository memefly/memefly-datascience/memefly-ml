import sacrebleu
from typing import List


def BLEU(*, refs:List, sys:List) -> float:
    """
    A comparable standard BLEU score using SacreBLEU paper library. The package solves
    the problem of incomparable BLEU scores across different papers due to different
    preprocessing steps taken on either source (training) or reference (target) data.
    https://www.aclweb.org/anthology/W18-6319.pdf
    https://github.com/mjpost/sacreBLEU

    Parameters:
    --------
    refs: a list of reference text strings lists
    sys: a list of inferenced text strings lists

    Outputs:
    --------
    output: BLEU score produced by SacreBleu package.
    """
    bleu = sacrebleu.corpus_bleu(sys, refs)

    return bleu.score

